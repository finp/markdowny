import * as fs from "fs";
import * as path from "path";
import * as yamlFrontMatter from "yaml-front-matter";

/**
* Parses the input files and returns a list of YAML metadata with special
* columns for calculated values.
*/
export function scanFiles(argv, files: string[])
{
    var list: any[] = [];

    for (var file of files)
    {
        // Load the metadata from the given file.
        var contents = fs.readFileSync(file, "utf8");
        var metadata = yamlFrontMatter.loadFront(contents);

        // Add in the standard fields as "_" entries.
        metadata._filename = file.replace(argv.prefix, "");
        metadata._basename = path.basename(file);
        metadata._words = metadata.__content
            .replace("'", "")
            .split(/\s+/g)
            .length;

        // Add the metadata to the list.
        list.push(metadata);
    }

    return list;
}
