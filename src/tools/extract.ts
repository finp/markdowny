import * as _ from "lodash";
import * as yargs from "yargs";
import * as scanner from "../scanner";

export var command = "extract";
export var describe = "Extracts a YAML from a list of files";

export function builder(yargs: yargs.Arguments)
{
    return yargs
        .help("help")

        .alias("o", "output")
        .default("output", "-")

        .demand(1);
}

export function handler(argv: any)
{
    var files = argv._.splice(1);
    var data = scanner.scanFiles(argv, files);
    render(argv, data);
}

export function render(argv, data)
{
    console.log(JSON.stringify(data));
}
