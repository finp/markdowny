import * as _ from "lodash";
import * as yargs from "yargs";
import * as scanner from "../scanner";

export var command = "list";
export var describe = "Extracts a YAML field into an order list";

export function builder(yargs: yargs.Arguments)
{
    return yargs
        .help("help")

        .alias("f", "field")
        .default("field", "summary")

        .alias("t", "title")
        .default("title", "title")

        .alias("o", "output")
        .default("output", "-")

        .demand(1);
}

export function handler(argv: any)
{
    var files = argv._.splice(1);
    var data = scanner.scanFiles(argv, files);
    render(argv, data);
}

export function render(argv, data)
{
    for (var key in data)
    {
        var number = 1 + parseInt(key.toString());
        var item = data[key];
        var title = _.get(item, argv.title);
        var value = _.get(item, argv.field);

        // Build up the parts so we can do this as a single line.
        const parts = [
            `${number}. `,
            title,
        ];

        if (value)
        {
            parts.push(": " + value.replace(/\n/g, " "));
        }

        // Write out the line.
        console.log(parts.join(""));
    }
}
