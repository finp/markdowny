markdowny
===========================

> A set of command-line utilities for working with Markdown + YAML files, specifically for novels and short stories.

## Setup and usage

Install `markdowny` using `npm` as a utility:

```sh
npm install --global markdowny
```

Basic usage is:

```sh
markdown verb *.markdown
```

## Verbs

### version

> Retrieves the version of the command-line utility and prints it out to the console.

```sh
markdowny version
```

### count

> Counts the number of words excluding the YAML header and displays it.

```sh
markdowny count *.markdown [options]
```

Options:

* `-t`, `--total`: Add a total line to the bottom that combines.
* `-s`, `--separator`: Comma-separates the numbers in the total.

### sections

> Displays each file as a heading 1 section with the title and contents provided.

Assuming that a file has a `title` and `summary` (which can be multiple lines) inside it, display a section for each file with the title as heading 1 and the contents of the `summary` value as the text for that section.

```sh
markdowny sections *.markdown -t title -f summary
```

Options:

* `-t`, `--title`: The name of the property to put in for the section. May be nested such as `author.name.first`. Defaults to `title`.
* `-f`, `--field`: The name of the property used for the contents of the section. Defaults to `summary`.

### table

> Displays a table of one or more fields inside the summary.

To list every file, it's title, the number of words inside it, and the list of secondary characters inside a nested value. The `:rs` makes the "Words" column right-aligned (`r`) and comma-separated (`s`).

```sh
markdowny table *.markdown -f _basename title _words characters.secondary -t File Title Words:rs "Secondary Characters"
```

Options:

* `-f`, `--fields`: A parameter-separated list of YAML fields to list in the table, in the order they should be shown. Defaults to `_basename title`. These may also have a format specifier, starting with `:` (see below). This may be a nested value, such as `characters.secondary`.
* `-t`, `--titles`: A parameter-separated list of titles for the columns. If there are more fields than titles, the field names will be used for the headers. These may have format specifiers.
* `--table-start`: The characters to insert at the beginning of the row. Defaults to `| `.
* `--table-end`: The characters to insert at the end of each row. Defaults to ` |`.
* `--table-delimiter`: The characters to insert between each field. Defaults to ` | `.
* `--no-table-rule`: Do not put a dashed line between the header and the various files.
* `--no-table-header`: Do not display the header titles. The format of the headers is still parsed and used.
* `--list-delimiter`: The characters to put between list items when inserted into a field. Defaults to `, `.
* `--prefix`: The filename prefix that is trimmed from the `_filename` propery.

There are a number of special properties that are included in all files.

* `_filename`: The full path of the filename, with the `--prefix` removed.
* `_basename`: The name of the file without a directory.
* `_words`: The number of words in the file, excluding the YAML header.
