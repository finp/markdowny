## [0.0.3](https://gitlab.com/mfgames-writing/markdowny/compare/v0.0.2...v0.0.3) (2018-08-11)


### Bug Fixes

* added package management ([c7c8281](https://gitlab.com/mfgames-writing/markdowny/commit/c7c8281))
